package chat

import (
	"log"
	"os"

	"golang.org/x/net/context"
)

type Server struct {
}

func (s *Server) SayHello(ctx context.Context, message *Message) (*Message, error) {
	log.Printf("Received message body from client %s", message.Body)
	hostname, _ := os.Hostname()
	return &Message{Body: "Hello from" + hostname}, nil
}
