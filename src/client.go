package main

import (
	"context"
	"log"
	"os"

	"app/src/chat"

	"github.com/joho/godotenv"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

func main() {
	_ = godotenv.Load(".env")
	var conn *grpc.ClientConn
	creds, _ := credentials.NewClientTLSFromFile("./certs/tls.crt", os.Getenv("DOMAIN"))
	conn, err := grpc.Dial("192.168.49.2:443", grpc.WithTransportCredentials(creds))
	if err != nil {
		log.Fatalf("could not connect %s", err)
	}
	defer conn.Close()

	chatClient := chat.NewChatServiceClient(conn)

	for i := 0; i < 1000; i += 1 {
		message := chat.Message{
			Body: "hello from client",
		}
		response, err := chatClient.SayHello(context.Background(), &message)
		if err != nil {
			log.Fatalf("Error when calling say hello %s", err)
		}
		log.Printf("Response: %s", response.Body)
	}
}
