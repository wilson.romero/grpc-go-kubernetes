package main

import (
	"fmt"
	"io"
	"net/http"
)

func index(response http.ResponseWriter, request *http.Request) {
	fmt.Println("Index")
	io.WriteString(response, "<h1>Index</h1>")
}

func healthCheck(response http.ResponseWriter, request *http.Request) {
	fmt.Println("Health")
	io.WriteString(response, "Healthy")
}

func main() {
	http.HandleFunc("/", index)
	http.HandleFunc("/health-check", healthCheck)
	http.ListenAndServe(":3000", nil)
	fmt.Println("Server listening...")
}
