package main

import (
	"app/src/chat"
	"log"
	"net"

	"google.golang.org/grpc"
)

func main() {
	lis, err := net.Listen("tcp", ":3000")
	if err != nil {
		log.Fatalf("Failed to listen on port 3000 %v", err)
	}
	grpcServer := grpc.NewServer()
	server := chat.Server{}
	chat.RegisterChatServiceServer(grpcServer, &server)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("Failed to serve on port 3000 %v", err)
	}
	log.Print("Listening on 3000")
}
